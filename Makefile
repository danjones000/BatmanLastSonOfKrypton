CI_COMMIT_REF_NAME ?= $(shell git rev-parse --abbrev-ref HEAD)
DEST := $(shell bash -c '[[ "${CI_COMMIT_REF_NAME}" = "master" ]] && echo public || echo public/${CI_COMMIT_REF_NAME}')

.PHONY: help deps build deploy

help: ## Print this help
	@echo "Make recipes:"
	@echo
	@grep -E '^[a-zA-Z0-9_#-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

deps: /usr/bin/rsync node_modules/ ## Install dependencies

/usr/bin/rsync:
	apt-get update
	apt-get install --no-install-recommends --no-install-suggests -y rsync
	apt-get remove -y
	apt-get autoremove -y
	apt-get clean
	rm -rf /var/lib/apt/lists/*

node_modules/: package.json yarn.lock
	yarn
	touch node_modules

build: _book/index.html ## Build the book

_book/index.html: node_modules/.bin/honkit *.md book.json
	npx honkit build

node_modules/.bin/honkit: node_modules/

deploy: $(DEST)/index.html ## Move built book to public directory, based on current branch

$(DEST)/index.html: /usr/bin/rsync _book/index.html
	mkdir -pv $(DEST)
	rm -rvf _book/public
	rsync -av _book/ $(DEST)/
